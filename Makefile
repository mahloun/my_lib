##
## Makefile for  in /home/nieto_t//fichiersc/Jour_11/ex_03
##
## Made by thomas nieto
## Login   <nieto_t@epitech.net>
##
## Started on  Tue Oct 23 12:34:56 2012 thomas nieto
## Last update Tue Jan 15 01:58:53 2013 thomas nieto
##

NAME		=

SRC 		= .c \
		main.c


OBJ		= $(SRC:.c=.o)

LIB		= my

PATHLIB		= my/

CC		= clang

MKLIB		= make -C my/

CFLAGS		+= -I my/ -Wall -Werror -W -g3

all: 		$(NAME)

$(NAME):	$(OBJ)
		@$(MKLIB)
		$(CC) -o $(NAME) $(OBJ) -l$(LIB) -L $(PATHLIB)

lib :
		@$(MKLIB)

home_copy:
		make -C my/ home_copy
		cp Makefile ~/my_lib/
clean:
		rm -f $(OBJ)
		make -C my/ clean

fclean:		clean
		rm -f $(NAME)
		make fclean -C my/

re:		fclean lib all

.PHONY:		all lib home_copy clean fclean re
