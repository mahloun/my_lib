/*
** my_strcpy.c for  in /home/nieto_t//fichiersc/Jour_06
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Oct  8 09:35:54 2012 thomas nieto
** Last update Sat Dec 22 16:49:41 2012 
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i += 1;
    }
  dest[i] = '\0';
  return (dest);
}
