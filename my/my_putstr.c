/*
** my_putstr.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:14:35 2012 thomas nieto
** Last update Tue Jan 15 00:09:00 2013 thomas nieto
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    my_putchar(str[i++]);
}

void	my_e_puterror(char *str)
{
  if ((write(2, str, my_strlen(str))) == -1)
    exit(EXIT_FAILURE);
  exit(EXIT_FAILURE);
}

int	my_r_puterror(char *str)
{
  if ((write(2, str, my_strlen(str))) == -1)
    exit(EXIT_FAILURE);
  return (0);
}
