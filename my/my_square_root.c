/*
** my_square_root.c for  in /home/nieto_t//fichiersc/Jour_05
** 
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
** 
** Started on  Wed Oct 17 20:35:33 2012 thomas nieto
** Last update Thu Oct 18 17:11:53 2012 thomas nieto
*/

int	my_square_root(int nb)
{
  int	sous;
  int	imp;

  imp = 1;
  sous = 0;
  while (nb >= imp)
    {
      nb = nb - imp;
      imp = imp + 2;
      sous = sous + 1;
    }
  if (nb == 0)
    return (sous);
  else
    return (0);
}
