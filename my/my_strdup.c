/*
** my_strdup.c for  in /home/nieto_t//fichiersc/Jour_08
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Wed Oct 10 18:43:42 2012 thomas nieto
** Last update Tue Jan 15 01:56:19 2013 thomas nieto
*/

#include <stdlib.h>
#include "my.h"

char	*my_strdup(char *src)
{
  char	*dest;

  dest = NULL;
  if ((dest = malloc(my_strlen(src) * sizeof(*dest))))
    my_strcpy(dest, src);
  else
    my_r_puterror("[ERROR]: (my_strdup) - malloc failed\n");
  return (dest);
}

char	*my_strndup(char *src, int i)
{
  char	*dest;

  dest = NULL;
  if ((dest = malloc((i) * sizeof(*dest))))
    {
      my_strncpy(dest, src, i);
      dest[i] = '\0';
    }
  else
    my_r_puterror("[ERROR]: (my_strndup) - malloc failed\n");
  return (dest);
}
