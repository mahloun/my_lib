/*
** print_char.c for  in /home/nieto_t//cours/system_unix/TP1
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Wed Nov 14 21:09:38 2012 thomas nieto
** Last update Sun Dec 23 18:22:18 2012 thomas nieto
*/

#include <stdarg.h>
#include "my.h"

void	print_char(va_list *ap, const char **fmt, long int **count)
{
  my_putchar(va_arg(ap, int));
  (**count)++;
}
