/*
** my_strcat.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:19:28 2012 thomas nieto
** Last update Sun Dec 23 18:19:36 2012 thomas nieto
*/

#include "my.h"

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	dest_len;

  i = 0;
  dest_len = my_strlen(dest);
  while (src [i] != '\0')
    {
      dest[dest_len] = src[i];
      i = i + 1;
      dest_len = dest_len + 1;
    }
  dest[dest_len + i] = '\0';
  return (dest);
}
