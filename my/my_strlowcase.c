/*
** my_strlowcase.c for  in /home/nieto_t//fichiersc/Jour_06
** 
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
** 
** Started on  Mon Oct  8 22:12:31 2012 thomas nieto
** Last update Mon Oct  8 22:20:23 2012 thomas nieto
*/

char	*my_strlowcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= 65 && str[i] <= 90)
	str[i] = str[i] + 32;
      i = i + 1;
    }
  return (str);
}
