/*
** my_strstr.c for  in /home/nieto_t//fichiersc/Jour_06
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Oct  8 13:49:29 2012 thomas nieto
** Last update Mon Oct 29 16:52:08 2012 thomas nieto
*/

char	*my_strstr(char *str, char *to_find)
{
  int	i;
  int	j;
  char	*result;

  i = 0;
  while (str[i] != '\0')
    {
      j = 0;
      if (to_find[j] == '\0')
	return (str);
      while (str[i] == to_find[j] && str[i] != '\0')
	{
	  i = i + 1;
	  j = j + 1;
	  if (to_find[j] == '\0')
	    {
	      result = str + (i - j);
	      return (result);
	    }
	}
      i = i + 1;
    }
  return ("\0");
}
