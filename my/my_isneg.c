/*
** my_isneg.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:12:49 2012 thomas nieto
** Last update Sun Dec 23 18:12:58 2012 thomas nieto
*/

#include "my.h"

void	my_isneg(int nb)
{
  if (nb < 0)
    my_putchar('N');
  else
    my_putchar('P');
}
