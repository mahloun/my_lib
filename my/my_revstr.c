/*
** my_revstr.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:15:11 2012 thomas nieto
** Last update Sun Dec 23 18:15:19 2012 thomas nieto
*/

#include "my.h"

char	*my_revstr(char *str)
{
  int	count;
  int	i;
  char	bidon;

  i = 0;
  count = my_strlen(str) - 1;
  while (count > i)
    {
      bidon = str[i];
      str[i] = str[count];
      str[count] = bidon;
      count = count - 1;
      i = i + 1;
    }
  return (str);
}
