/*
** m_printf.h for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <tinh@epitech.net>
**
** Started on Sun Dec 23 18:07:00 2012 thomas nieto
** Last update Sun Dec 23 18:07:02 2012 thomas nieto
*/

#ifndef M_PRINTF_H_
# define M_PRINTF_H_

# include <stdarg.h>

# define NBR_FLAGS 12

long int	my_printf(const char *, ...);
void		print_string(va_list *, const char **, long int **);
void		print_char(va_list *, const char **, long int **);
void		print_nbr_d_i_u(va_list *, const char **, long int **);
void		print_nbr_b_o(va_list *, const char **, long int **);
void		print_nbr_x_X(va_list *, const char **, long int **);
void		print_float(va_list *, const char **, long int **);
void		print_ptr_adr(va_list *, const char **, long int **);
void		print_special(va_list *, const char**, long int **);

typedef struct	s_flags
{
    const char	c;
    void	(*do_flags)(va_list *, const char **, long int **);
}		t_flags;

#endif /* !M_PRINTF_H_ */
