/*
** my_str_islower.c for  in /home/nieto_t//fichiersc/Jour_06
** 
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
** 
** Started on  Tue Oct  9 12:34:18 2012 thomas nieto
** Last update Tue Oct  9 14:10:16 2012 thomas nieto
*/

int	my_str_islower(char *str)
{
  int	i;
  int	res;

  i = 0;
  res = 1;
  if (str[i] == '\0')
    return (1);
  while (str[i] != '\0')
    {
      if (str[i] < 96 || str[i] > 123)
	return (0);
      i = i + 1;
    }
  return (res);
}
