/*
** my.h for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:07:13 2012 thomas nieto
** Last update Sat Dec 29 23:08:11 2012 thomas nieto
*/

#ifndef MY_H_
# define MY_H_

# define TRUE (1)
# define FALSE (!TRUE)

# define RD_ERROR "Error during read process\n"
# define OP_ERROR "Can't open file\n"
# define WR_ERROR "Error during write process\n"
# define AL_ERROR "Error during malloc process\n"

void		my_putchar(char);
void		my_isneg(int);
char		*my_put_nbr(int);
int		my_swap(int *, int *);
void		my_putstr(char *);
void		my_e_puterror(char *);
int		my_r_puterror(char *);
int		my_strlen(char	*);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
int		my_power_rec(int, int);
int		my_square_root(int);
int		my_is_prime(int);
int		my_find_prime_sup(int);
char		*my_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_revstr(char *);
char		*my_strdup(char *);
char		*my_strndup(char *, int);
char		*my_strstr(char *, char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*my_strcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		my_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
void		my_showstr(char *);
int		my_showmem(char *, int);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
char		*my_putnbr_base(long int, char *);
char		*my_putnbr_base_adr(void *, char *);
char		**my_str_to_wordtab(char *, char);
char		**my_wordtab_realloc(char **);
int		lengh_wordtab(char **);
void		my_show_wordtab(char **);
char		*sum_params(int, char **);
int		size_argvs(int,	char **, int);
int		my_getnbr_base(char *, char *);
int		free_wordtab(char **);

#endif /* !MY_H_ */
