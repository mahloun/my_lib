/*
** my_strncmp.c for  in /home/nieto_t//fichiersc/Jour_06
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Oct  8 21:49:18 2012 thomas nieto
** Last update Sun Dec 30 16:14:30 2012 thomas nieto
*/

#include "my.h"

int	my_strncmp(char *s1, char *s2, int nb)
{
  int	i;

  i = 0;
  while (nb > 0 && s1[i] != '\0' && s2[i] != '\0')
    {
      if (s1[i] != s2[i])
	return (s1[i]  - s2[i]);
      i = i + 1;
      nb = nb - 1;
    }
  return (0);
}
