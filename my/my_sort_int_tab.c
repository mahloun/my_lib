/*
** my_sort_int_tab.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:17:22 2012 thomas nieto
** Last update Sun Dec 23 18:17:30 2012 thomas nieto
*/

#include "my.h"

void	my_sort_int_tab(int *tab, int size)
{
  int	i;
  int	j;

  i = 0;
  while (i < size)
    {
      j = 0;
      while (j < size - 1)
	{
	  if (tab[j] > tab[j + 1])
	    my_swap(&tab[j], &tab[j + 1]);
	  j = j + 1;
	}
      i = i + 1;
    }
}
