/*
** my_find_prime_sup.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:11:39 2012 thomas nieto
** Last update Sun Dec 23 18:11:47 2012 thomas nieto
*/

#include "my.h"

int	my_find_prime_sup(int nb)
{
  while (!my_is_prime(nb))
    {
      nb = nb + 1;
      my_is_prime(nb);
    }
  return (nb);
}
