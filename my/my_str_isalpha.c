/*
** my_str_isalpha.c for  in /home/nieto_t//fichiersc/Jour_06
** 
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
** 
** Started on  Tue Oct  9 12:11:21 2012 thomas nieto
** Last update Fri Oct 12 18:02:36 2012 thomas nieto
*/

int	my_str_isalpha(char *str)
{
  int	i;
  int	res;

  i = 0;
  res = 1;
  while (str[i] != '\0')
    {
      if ((str[i] < 96 || str[i] > 123)
	  || (str[i] < 64 || str[i] > 91))
	return (0);
      i = i + 1;
    }
  return (res);
}
