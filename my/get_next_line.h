/*
** get_next_line.h for Epitech in /home/tinh/git_rendus/get_next_line
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sat Jan 12 13:15:27 2013 thomas nieto
** Last update Tue Jan 15 01:59:15 2013 thomas nieto
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

#define	BUFF_SIZE 256

char	*get_next_line(const int);
void	*my_realloc(void *, int);

#endif /* !GET_NEXT_LINE_H_ */
