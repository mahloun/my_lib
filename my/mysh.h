/*
** mysh.h for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:20:44 2012 thomas nieto
** Last update Sun Dec 30 04:48:25 2012 thomas nieto
*/

#ifndef MYSH_H_
# define MYSH_H_

# define BUFF_SIZE 256
# define NBR_BUILTINS 4

#include <unistd.h>

void	init_command(char **, char **, char *[]);
int	builtins(char **, char **[]);

typedef struct	s_builtins
{
    char	*name;
    void	(*fct_builtins)(char **, char **[]);
}		t_builtins;

void	help_builtin(char **, char **[]);
void	cd_builtin(char **, char **[]);
void	setenv_builtin(char **, char **[]);
void	unsetenv_builtin(char **, char **[]);

#endif /* !MYSH_H_ */
