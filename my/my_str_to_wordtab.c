/*
** my_str_to_wordtab.c for  in /home/tinh/my_str_to_wordtab
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 16:13:12 2012 thomas nieto
** Last update Thu Dec 27 11:04:21 2012 thomas nieto
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "my.h"

void		calc_size_wordtab(char *str, int *size_wordtab, char delim)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] && str[i] != delim)
	{
	  while (str[i] && str[i] != delim)
	    i += 1;
	  *size_wordtab += 1;
	}
      if (str[i])
	i += 1;
    }
}

char		*fill_wordtab(char *str, int *k, char delim)
{
  char		*word;
  int		j;

  word = NULL;
  j = 0;
  while (str[*k] && str[*k] == delim)
    *k += 1;
  str += *k;
  while (str[j] && str[j] != delim)
    j += 1;
  *k += j;
  word = my_strndup(str, j);
  return (word);
}

char		**my_str_to_wordtab(char *str, char delim)
{
  char		**wordtab;
  int		i;
  int		k;
  int		size_wordtab;

  i = 0;
  k = 0;
  size_wordtab = 0;
  wordtab = NULL;
  calc_size_wordtab(str, &size_wordtab, delim);
  if ((wordtab = malloc((size_wordtab + 1) * sizeof(*wordtab))))
    {
      while (i < size_wordtab)
	wordtab[i++] = fill_wordtab(str, &k, delim);
      wordtab[size_wordtab] = NULL;
    }
  else
    {
      my_r_puterror("[ERROR]: (my_str_to_wordtab) -  malloc failed\n");
      return (NULL);
    }
  return (wordtab);
}
