/*
** my_putchar.c for  in /home/nieto_t//my_lib
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Tue Oct  9 18:09:46 2012 thomas nieto
** Last update Tue Jan 15 00:32:49 2013 thomas nieto
*/

#include <unistd.h>
#include "my.h"

void	my_putchar(char c)
{
  if ((write(1, &c, 1)) == -1)
    my_e_puterror(RD_ERROR);
}
