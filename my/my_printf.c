/*
** print_string.c for  in /home/nieto_t//cours/system_unix/TP1
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Tue Nov 13 16:01:06 2012 thomas nieto
** Last update Fri Nov 23 13:47:04 2012 thomas nieto
*/

#include <stdarg.h>
#include <stdlib.h>
#include "my.h"
#include "m_printf.h"

static const t_flags	g_tab_flags[NBR_FLAGS] =
{
  {'c', &print_char},
  {'d', &print_nbr_d_i_u},
  {'i', &print_nbr_d_i_u},
  {'u', &print_nbr_d_i_u},
  {'b', &print_nbr_b_o},
  {'o', &print_nbr_b_o},
  {'x', &print_nbr_x_X},
  {'X', &print_nbr_x_X},
  {'p', &print_ptr_adr},
  {'s', &print_string},
  {'S', &print_string},
  {'%', &print_special},
};

int		sort_flags(const char **fmt)
{
  int		i;

  i = 0;
  (*fmt)++;
  while ((**fmt >= '0' && **fmt <= '9') || **fmt == '.'
	 || **fmt == '-' || **fmt == '+' || **fmt == '#')
      (*fmt)++;
  while (**fmt != g_tab_flags[i].c && i <= NBR_FLAGS)
    i++;
  return (i);
}

int		do_fmt_lengh(const char *fmt)
{
  int		count;

  count = 0;
  while (*fmt)
    {
      if (*fmt == '%')
	{
	  sort_flags(&fmt);
	  fmt++;
	}
      count++;
      fmt++;
    }
  return (count);
}

const char	*sous_my_printf(va_list *ap, const char **fmt, long int *count)
{
  int		i;

  i = 0;
   (*fmt)++;
  while ((**fmt >= '0' && **fmt <= '9') || **fmt == '.'
  	 || **fmt == '-' || **fmt == '+' || **fmt == '#')
    (*fmt)++;
  while (**fmt != g_tab_flags[i].c && i <= NBR_FLAGS)
    i++;
  if (i <= NBR_FLAGS)
    g_tab_flags[i].do_flags(ap, fmt, &count);
  else
    {
      my_putchar(*(*fmt - 1));
      my_putchar(**fmt);
    }
  (*fmt)++;
  return (*fmt);
}

long int	my_printf(const char *fmt, ...)
{
  va_list	ap;
  long int	ret_value;
  int		fmt_lengh;
  long int	count_fct;

  ret_value = 0;
  count_fct = 0;
  va_start(ap, fmt);
  fmt_lengh = do_fmt_lengh(fmt);
  while (*fmt)
    {
      if (*fmt == '%')
	sous_my_printf(&ap, &fmt, &count_fct);
      else
	{
	  my_putchar(*(fmt++));
	  ret_value++;
	}
    }
  if (ret_value == fmt_lengh)
    ret_value += count_fct;
  else
    ret_value == -1;
  va_end(ap);
  return (ret_value);
}
