/*
** my_str_isprintable.c for  in /home/nieto_t//fichiersc/Jour_06
** 
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
** 
** Started on  Tue Oct  9 12:42:11 2012 thomas nieto
** Last update Tue Oct  9 14:21:00 2012 thomas nieto
*/

int	my_str_isprintable(char *str)
{
  int	i;
  int	res;

  i = 0;
  res = 1;
  if (str[i] == '\0')
    return (1);
  while (str[i] != '\0')
    {
      if (str[i] < 31 || str[i] > 127)
	return (0);
      i = i + 1;
    }
  return (res);
}
