/*
** my_strncpy.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:19:51 2012 thomas nieto
** Last update Sun Dec 23 18:19:58 2012 thomas nieto
*/

#include "my.h"

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;
  int	srclen;

  i = 0;
  srclen = my_strlen(src);
  while (n > 0 && src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
      n = n - 1;
    }
  if (n > srclen)
    dest[i] = '\0';
  return (dest);
}
