/*
** my_realloc.c for  in /home/tinh/git_rendus/get_next_line
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sat Jan 12 13:39:41 2013 thomas nieto
** Last update Tue Jan 15 01:37:04 2013 thomas nieto
*/

#include <stdlib.h>
#include "my.h"

void		*my_realloc(void *data, int size)
{
  void		*new_data;

  new_data = NULL;
  if (!data)
    return (malloc(size * sizeof(*data)));
  else
    {
      if ((new_data = malloc(size * sizeof(*data))))
	{
	  my_strncpy(new_data, data, size);
	  free(data);
	  return (new_data);
	}
      else
	return (data);
    }
}
