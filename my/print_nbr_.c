/*
** print_nbr_.c for  in /home/nieto_t//cours/system_unix/TP1
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Wed Nov 14 21:09:26 2012 thomas nieto
** Last update Sun Nov 18 15:39:08 2012 thomas nieto
*/

#include <stdarg.h>
#include <stdlib.h>
#include "my.h"

void	print_nbr_d_i_u(va_list *ap, const char **fmt, long int **count)
{
  char	*nbr;

  if (**fmt == 'd')
    nbr = my_put_nbr(va_arg(ap, int));
  else if (**fmt == 'i')
    nbr = my_put_nbr(va_arg(ap, int));
  else
    nbr = my_putnbr_base(va_arg(ap, unsigned int), "0123456789");
  my_putstr(nbr);
  **count += my_strlen(nbr);
  free(nbr);
}

void	print_nbr_b_o(va_list *ap, const char **fmt, long int **count)
{
  char	*nbr;

  nbr = NULL;
  if (**fmt == 'b')
    nbr = my_putnbr_base(va_arg(ap, int), "01");
  else
    nbr = my_putnbr_base(va_arg(ap, int), "01234567");
  my_putstr(nbr);
  **count += my_strlen(nbr);
  free(nbr);
}

void	print_nbr_x_X(va_list *ap, const char **fmt, long int **count)
{
  char	*nbr;

  nbr = NULL;
  if (**fmt == 'x')
    nbr = my_putnbr_base(va_arg(ap, long int), "0123456789abcdef");
  else
    nbr = my_putnbr_base(va_arg(ap, long int), "0123456789ABCDEF");
  **count += my_strlen(nbr);
  my_putstr(nbr);
  free(nbr);
}
