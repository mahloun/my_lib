/*
** my_strlcat.c for  in /home/nieto_t//fichiersc/Jour_07
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Wed Oct 10 12:11:26 2012 thomas nieto
** Last update Wed Dec 12 16:58:04 2012 tinh
*/

#include "my.h"

int	my_strlcat(char *dest, char *src, int size)
{
  int	i;
  int	dest_len;

  i = 0;
  dest_len = my_strlen(dest);
  while (i < size)
    {
      dest[dest_len] = src[i];
      i = i + 1;
      dest_len = dest_len + 1;
    }
  dest[i] = '\0';
  return (my_strlen(dest) + 1);
}
