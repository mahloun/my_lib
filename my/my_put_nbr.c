/*
** my_put_nbr.c for $ in /home/nieto_t//fichiersc/Jour_03
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Oct 15 11:39:08 2012 thomas nieto
** Last update Tue Dec  4 21:38:44 2012 thomas nieto
*/

#include <stdlib.h>
#include "my.h"

void	negative_nbr(int *nbr)
{
  if (*nbr < 0)
    {
      my_putchar('-');
      *nbr = -(*nbr);
    }
}

int	size_nbr(int nbr)
{
  int	i;

  i = 0;
  while (nbr > 0)
    {
      nbr = nbr / 10;
      i++;
    }
  return (i);
}

void	char_nbr(int nbr, int *i, char *n_nbr)
{
  int	rest_nbr;

  while (nbr > 0)
    {
      rest_nbr = nbr % 10;
      n_nbr[*i] = rest_nbr + '0';
      nbr = nbr / 10;
      (*i)++;
    }
}

char	*my_put_nbr(int nbr)
{
  char	*n_nbr;
  int	i;

  i = 0;
  negative_nbr(&nbr);
  n_nbr = malloc((size_nbr(nbr) + 1) * sizeof(*n_nbr));
  if (n_nbr == NULL)
    return NULL;
  if (nbr == 0)
    n_nbr = my_strdup("0");
  else if (nbr == -2147483648)
    n_nbr = my_strdup("2147483648");
  else
    {
      char_nbr(nbr, &i, n_nbr);
      n_nbr[i] = '\0';
      my_revstr(n_nbr);
    }
  return (n_nbr);
}
