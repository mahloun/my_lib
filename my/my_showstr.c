/*
** my_showstr.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:16:49 2012 thomas nieto
** Last update Sun Dec 23 18:17:06 2012 thomas nieto
*/

#include <stdlib.h>
#include "my.h"

int	my_char_isprintable(char carac)
{
  if (carac < 31 || carac > 127)
    return (0);
  return (1);
}

void	my_showstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (!my_char_isprintable(str[i]))
	{
	  my_putchar('\\');
	  if (str[i] < 7)
	    my_putstr("00");
	  if (str[i] >= 8)
	    my_putchar('0');
	  my_putstr(my_putnbr_base(str[i], "01234567"));
	  i = i + 1;
	}
      if (str[i] == '\0')
	return ;
      else
	my_putchar(str[i]);
      i = i + 1;
    }
}
