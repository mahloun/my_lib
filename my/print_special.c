/*
** print_printed.c for  in /home/nieto_t//cours/system_unix/TP1/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Thu Nov 15 18:41:48 2012 thomas nieto
** Last update Sun Nov 18 12:18:34 2012 thomas nieto
*/

#include <stdarg.h>

void	print_special(va_list *ap, const char **fmt, long int **count)
{
  if (**fmt == '%')
    my_putchar(**fmt);
}
