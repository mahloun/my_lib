/*
** my_is_prime.c for  in /home/nieto_t//fichiersc/Jour_05
** 
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
** 
** Started on  Thu Oct 18 00:36:40 2012 thomas nieto
** Last update Thu Oct 18 17:16:13 2012 thomas nieto
*/

int	my_prime_root(int nb)
{
  int	sous;
  int	imp;

  imp = 1;
  sous = 0;
  while (nb >= imp)
    {
      nb = nb - imp;
      imp = imp + 2;
      sous = sous + 1;
    }
  return (sous);
}

int	my_is_prime(int	nb)
{
  int	prime;
  int	nb_sqrt;

  if (nb <= 1)
    return (0);
  prime = 2;
  nb_sqrt = my_prime_root(nb);
  while (prime <= nb_sqrt)
    {
      if (nb % prime == 0)
	return (0);
      prime = prime + 1;
    }
  return (1);
}
