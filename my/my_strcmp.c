/*
** my_strcmp.c for  in /home/nieto_t//fichiersc/Jour_06
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Mon Oct  8 18:50:32 2012 thomas nieto
** Last update Mon Oct 29 16:51:26 2012 thomas nieto
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] != '\0' || s2[i] != '\0')
    {
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
      i = i + 1;
    }
  return (0);
}
