/*
** print_ptr_adr.c for  in /home/nieto_t//cours/system_unix/TP1
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Tue Nov 13 21:52:05 2012 thomas nieto
** Last update Sun Nov 18 15:40:38 2012 thomas nieto
*/

#include <stdarg.h>
#include <stdlib.h>
#include "my.h"

void	print_ptr_adr(va_list *ap, const char **fmt, long int **count)
{
  char	*adr;

  adr = my_putnbr_base_adr(va_arg(ap, void *), "0123456789abcdef");
  my_putstr(adr);
  **count += my_strlen(adr);
  free(adr);
}
