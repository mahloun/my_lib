/*
** my_putnbr_base_adr.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <tinh@epitech.net>
**
** Started on Sun Dec 23 18:14:11 2012 thomas nieto
** Last update Sun Dec 23 18:14:12 2012 thomas nieto
*/

/*
** long int = −9 223 372 036 854 775 808 to +9 223 372 036 854 775 807
*/

#include <stdlib.h>
#include "my.h"

int		negative_adr(long int *nbr)
{
  if (*nbr < 0)
    {
      *nbr = -(*nbr);
      my_putchar('-');
      return (0);
    }
  else
    return (1);
}

int		size_adr(long int trans_nbr, long int nbase)
{
  int		i;

  i = 0;
  while (trans_nbr > 0)
    {
      trans_nbr = trans_nbr / nbase;
      i = i + 1;
    }
  return (i);
}

char		*my_putnbr_base_adr(void *nbr, char *base)
{
  long int	nbase;
  int		i;
  long int	trans_nbr;
  int		rest_nbr;
  char		*n_nbr;

  i = 0;
  trans_nbr = (long int)nbr;
  nbase = my_strlen(base);
  negative_adr(&trans_nbr);
  n_nbr = malloc(size_adr(trans_nbr, nbase) * sizeof(*n_nbr));
  my_putstr("0x");
  while (trans_nbr > 0)
    {
      rest_nbr = (trans_nbr % nbase);
      n_nbr[i] = base[rest_nbr];
      trans_nbr = trans_nbr / nbase;
      i = i + 1;
    }
  my_revstr(n_nbr);
  return (n_nbr);
}
