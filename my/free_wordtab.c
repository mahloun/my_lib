/*
** free_wordtab.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:06:21 2012 thomas nieto
** Last update Wed Dec 26 00:04:37 2012 thomas nieto
*/

#include <stdlib.h>

void	free_wordtab(char **wordtab)
{
  int	i;

  i = 0;
  while (wordtab[i] != NULL)
    {
      free(wordtab[i]);
      i += 1;
    }
  free(wordtab);
}
