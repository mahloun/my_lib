/*
** my_realloc.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sat Dec 29 21:48:21 2012 thomas nieto
** Last update Sat Dec 29 22:56:52 2012 thomas nieto
*/

#include <stdlib.h>
#include "my.h"

char		**my_wordtab_realloc(char **wordtab)
{
  int		i;
  char		**new_wordtab;

  i = lengh_wordtab(wordtab);
  if (new_wordtab = malloc((i + 1) * sizeof(*new_wordtab)))
    {
      i = 0;
      while (wordtab[i])
	new_wordtab[i] = my_strdup(wordtab[i++]);
      new_wordtab[i] = NULL;
    }
  else
    {
      my_r_puterror("[ERROR]: my_realloc failed\n");
      return (NULL);
    }
  return (new_wordtab);
}
