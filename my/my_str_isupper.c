/*
** my_str_isupper.c for  in /home/nieto_t//fichiersc/Jour_06
** 
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
** 
** Started on  Tue Oct  9 12:40:21 2012 thomas nieto
** Last update Fri Oct 12 01:01:50 2012 thomas nieto
*/

int	my_str_isupper(char *str)
{
  int	i;
  int	res;

  i = 0;
  res = 1;
  if (str[i] == '\0')
    return (1);
  while (str[i] != '\0')
    {
      if (str[i] < 64 || str[i] > 91)
	return (0);
      i = i + 1;
    }
  return (res);
}
