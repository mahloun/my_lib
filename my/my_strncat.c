/*
** my_strncat.c for  in /home/nieto_t//fichiersc/Jour_07
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Wed Oct 10 11:57:02 2012 thomas nieto
** Last update Wed Dec 12 16:58:24 2012 tinh
*/

#include "my.h"

char	*my_strncat(char *dest, char *src, int nb)
{
  int	i;
  int	dest_len;

  i = 0;
  dest_len = my_strlen(dest);
  while (src [i] != '\0' && nb > 0)
      {
	dest[dest_len] = src[i];
	i = i + 1;
	dest_len = dest_len + 1;
	nb = nb - 1;
      }
    dest[dest_len + i] = '\0';
  return (dest);
}
