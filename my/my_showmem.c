/*
** my_showmem.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:16:24 2012 thomas nieto
** Last update Sun Dec 23 18:16:35 2012 thomas nieto
*/

#include "my.h"

void	adr_hex(int line)
{
  if (line < 1)
    my_putstr("00000000: ");
  else if (line > 0 && line < 256)
    {
      my_putstr("000000");
      my_putnbr_base(line, "0123456789abcdef");
      my_putstr(": ");
    }
  else
    {
      my_putstr("00000");
      my_putnbr_base(line, "0123456789abcdef");
      my_putstr(": ");
    }
}

void	my_show_str(char *str, int line)
{
  int	i;

  i = line;
  while ((i < line + 16) && str[i] != '\0')
    {
      if (str[i] < 31 || str[i] > 127)
	my_putchar('.');
      else
	my_putchar(str[i]);
      i = i + 1;
    }
  my_putchar('\n');
}

void	my_ascii_carac(char *str, int *line)
{
  int	i;

  i = *line;
  while (str[i] != '\0' && i < *line + 16)
    {
      if (str[i] < 16)
	my_putchar('0');
      my_putnbr_base(str[i], "0123456789abcdef");
      if (i % 2 != 0)
	my_putchar(' ');
      i = i + 1;
    }
  while (i < *line + 16)
    {
      my_putstr("  ");
      if (i % 2 != 0)
	my_putchar(' ');
      i = i + 1;
    }
  my_show_str(str, *line);
}

int	my_showmem(char *str, int size)
{
  int	i;
  int	line;

  i = 0;
  line = 0;
  while (line < size && str[i] != '\0')
    {
      adr_hex(line);
      my_ascii_carac(str, &line);
      i = i + 1;
      line = i * 16;
    }
  return (0);
}
