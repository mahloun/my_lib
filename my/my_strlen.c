/*
** my_strlen.c for  in /home/nieto_t//fichiersc/Jour_04
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Thu Oct  4 19:10:45 2012 thomas nieto
** Last update Tue Jan 15 00:35:04 2013 thomas nieto
*/

#include "my.h"

int	my_strlen(char *str)
{
 int	i;

  i = 0;
  while (str[i])
      i += 1;
  return (i);
}
