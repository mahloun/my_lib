/*
** my_getnbr_base.c for  in /home/nieto_t//fichiersc/Jour_06
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Sat Oct 20 18:57:21 2012 thomas nieto
** Last update Sun Dec  2 19:07:05 2012 thomas nieto
*/

#include <stdlib.h>
#include "my.h"

int	error_str_not_in_base(char *str, char *base)
{
  int	i;
  int	j;

  i = 0;
  while (str[i] != '\0')
    {
      j = 0;
      while (str[i] != base[j])
	{
	  if (base[j] == '\0')
	    return (0);
	  j = j + 1;
	}
      i = i + 1;
    }
  return (1);
}

int	error(char *str, char *base)
{
  int	i;
  int	j;

  i = 0;
  if (str[i] == '\0' || base[i] == '\0')
    return (0);
  my_getnbr(str);
  while (str[i] == '+' || str[i] == '-')
    i = i + 1;
  if (str[i - 1] == '-')
    my_putchar('-');
  if (!error_str_not_in_base(str, base))
    return (0);
  while (base[i] != '\0')
    {
      j = i + 1;
      while (base[j] != '\0')
	{
	  if (base[i] == base[j])
	    return (0);
	  j = j + 1;
	}
      i = i  + 1;
    }
   return (1);
}

int	my_getnbr_base(char *str, char *base)
{
  int	nbr;

  if (!error(str, base))
    return (0);
  nbr = my_getnbr(str);
  return (nbr);
}
