/*
** my_swap.c for  in /home/nieto_t//fichiersc/Jour_04
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Thu Oct  4 18:23:05 2012 thomas nieto
** Last update Sun Dec 23 18:20:32 2012 thomas nieto
*/

void	my_swap(int *a, int *b)
{
  int	null;

  null = *a;
  *a = *b;
  *b = null;
}
