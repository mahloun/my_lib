/*
** lengh_wordtab.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sat Dec 29 22:56:15 2012 thomas nieto
** Last update Sat Dec 29 22:56:51 2012 thomas nieto
*/

#include "my.h"

int		lengh_wordtab(char **wordtab)
{
  int		i;

  i = 0;
  while (wordtab[i])
    i += 1;
  return (i + 1);
}
