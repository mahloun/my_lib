/*
** print_string.c for  in /home/nieto_t//cours/system_unix/TP1
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Tue Nov 13 16:01:06 2012 thomas nieto
** Last update Sun Nov 18 15:40:32 2012 thomas nieto
*/

#include <stdarg.h>

void	print_string(va_list *ap, const char **fmt, long int **count)
{
  char	*str;

  if (**fmt == 's')
    {
      str = va_arg(ap, char *);
      my_putstr(str);
    }
  else
    my_showstr(va_arg(ap, char *));
  **count += my_strlen(str);
}
