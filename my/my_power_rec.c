/*
** my_power_rec.c for  in /home/nieto_t//fichiersc/Jour_05
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on  Fri Oct  5 21:59:08 2012 thomas nieto
** Last update Mon Oct 29 16:53:44 2012 thomas nieto
*/

int	my_power_rec(int nb, int power)
{
  int	init;

  init = nb;
  if (power == 0)
    return (1);
  if (power > 1)
    init = init * my_power_rec(nb, power - 1);
  return (init);
}
