/*
** my_getnbr.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:12:02 2012 thomas nieto
** Last update Sun Dec 23 18:12:11 2012 thomas nieto
*/

int	my_getnbr(char *str)
{
  int	nbr;
  int	i;
  int	j;

  i = 0;
  j = 1;
  nbr = 0;
  while ((str[i] != '\0' && str[i] == '+') ||
	 str[i] == '-' || (str[i] >= '0' && str[i] <= '9'))
    {
      if (str[i] >= '0' && str[i] <= '9')
	{
	  nbr = nbr * 10 + (str[i] - 48);
	  j = j + 1;
	}
      i = i + 1;
    }
  if (str[i - j] == '-')
    return (-nbr);
  else
    return (nbr);
}
