/*
** my_putnbr_base.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:13:51 2012 thomas nieto
** Last update Sun Dec 23 18:13:58 2012 thomas nieto
*/

#include <stdlib.h>
#include "my.h"

int		negative(long int *nbr)
{
  if (*nbr < 0)
    {
      *nbr = -(*nbr);
      my_putchar('-');
      return (0);
    }
  else
    return (1);
}

int		size(long int trans_nbr, int nbase)
{
  int		i;

  i = 0;
  while (trans_nbr > 0)
    {
      trans_nbr = trans_nbr / nbase;
      i = i + 1;
    }
  return (i);
}

char		*my_putnbr_base(long int nbr, char *base)
{
  int		nbase;
  int		i;
  long	int	trans_nbr;
  int		rest_nbr;
  char		*n_nbr;

  i = 0;
  trans_nbr = nbr;
  nbase = my_strlen(base);
  negative(&trans_nbr);
  n_nbr = malloc(size(trans_nbr, nbase) * sizeof(*n_nbr));
  if (n_nbr == NULL)
    return (0);
  while (trans_nbr > 0)
    {
      rest_nbr = (trans_nbr % nbase);
      n_nbr[i] = base[rest_nbr];
      trans_nbr = trans_nbr / nbase;
      i = i + 1;
    }
  my_revstr(n_nbr);
  return (n_nbr);
}
