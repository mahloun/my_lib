/*
** my_strcapitalize.c for  in /home/tinh/rendus_svn/minishell1-2017-nieto_t/my
**
** Made by thomas nieto
** Login   <nieto_t@epitech.net>
**
** Started on Sun Dec 23 18:19:05 2012 thomas nieto
** Last update Sun Dec 23 18:19:15 2012 thomas nieto
*/

char	*my_strcapitalize(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      while (str[i] != '\0' && (str[i] < 'a' || str[i] > 'z')
	     && (str[i] < 'A' || str[i] > 'Z')
	     && (str[i] < '0' || str[i] > '9'))
	i = i + 1;
      if (str[i] >= 'a' && str[i] <= 'z'
	  && (str[i - 1] < '0' || str[i - 1] > '9'))
	str[i] = str[i] - 32;
      i = i + 1;
      while (str[i] != '\0' && str[i] != ' ' &&
	     str[i] != '+' && str[i] != '-')
	{
	  if (str[i] >= 'A' && str[i] <= 'Z')
	    str[i] = str[i] + 32;
	  i = i + 1;
	}
    }
  return (str);
}
